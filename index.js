'use strict'

exports.expect = (expression) => {
  return {
    true() {
      x(expression === true, ' to be true')
    },
    truthy() {
      x(!!expression, ' to be truthy')
    },
    false() {
      x(expression === false, ' to be false')
    },
    falsey() {
      x(!expression, ' to be falsey')
    },
    equal(value) {
      x(deepCompare(expression, value), ' to equal ' + value)
    },
    notequal(value) {
      x(!deepCompare(expression, value), ' to not equal ' + value)
    }
  }
  
  function x (testPassed, message) {
    if (!testPassed)
      throw new Error('Expected ' + expression + message)
  }
  
  function deepCompare (obj1, obj2) {
    if (Array.isArray(obj1) && Array.isArray(obj2)) {
      return obj1.every((val, index) => 
        deepCompare(val, obj2[index])
      )
    }

    else if (isPlainObject(obj1) && isPlainObject(obj2)) {
      let keys1 = Object.keys(obj1)
      ,   keys2 = Object.keys(obj2)
      
      if (keys1.length !== keys2.length) return false
      return keys1.every(key => 
        deepCompare(obj1[key], obj2[key])
      )
    }

    else
      return obj1 === obj2
    
    function isPlainObject (obj) {
      return typeof obj === 'object' && !Array.isArray(obj)
    }
  }
}

