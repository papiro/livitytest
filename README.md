# Livity-Test

Livity-Test is meant to be a minimalist set of tools to aid in the testing of node modules.

It is _not_ verbose :)

The general idea is that you stack all of your tests like you normally would in some tests.js file which you would run via npm test or whatever.
The goal is __no output__.  No output means every test passed.  

If a test fails, the harness will exit without executing any further tests.  It was chosen to be designed this way!

It will tell you the line number of the test which failed, via the Node.js standard error stack trace.

Fix it, and then resume your tests.

It's minimal!  But it works well :D
