'use strict'

const expect = require('../index').expect

expect(true).true()
expect({}).truthy()
expect('s').truthy()
expect(false).false()
expect('').falsey()
expect(true).equal(true)
expect(false).equal(false)
expect([1,2,3]).equal([1, 2, 3])
expect({val:'prop'}).equal({val:'prop'})
let deepObj1 = {
  prop: {
    subprop1: 'val',
    subprop2: 'val2'
  }
}
,   deepObj2 = {
  prop: {
    subprop1: 'val',
    subprop2: 'val2'
  }
}
,   deepObj3 = {
  prop: {
    subprop1: 'val1',
    subprop2: 'val2'
  }
}
expect(deepObj1).equal(deepObj2)
expect([deepObj1,deepObj2]).equal([deepObj1, deepObj2])
expect(true).notequal(false)
expect('').notequal(false)
expect(deepObj2).notequal(deepObj3)
expect([deepObj1,deepObj2]).notequal([deepObj1,deepObj3])

